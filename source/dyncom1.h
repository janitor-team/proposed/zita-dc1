// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#ifndef __DYNCOM1_H
#define __DYNCOM1_H


#include <stdint.h>
#include "global.h"


class Dyncom1
{
public:

    Dyncom1 (int fsamp, int nchan);
    ~Dyncom1 (void);
    
    void set_ipgain (float ipgdb)
    {
        _ipg1 = powf (10.0f, 0.05f * ipgdb);
    }
    void set_ratio (float ratio)
    {
        _rat1 = 0.5f * ratio; 
    }
    void set_thres (float thres)
    {
        _pthr = 0.5f * powf (10.0f, 0.1f * thres);
    }
    void set_tatt (float tatt)
    {
        _tatt = tatt;
    }
    void set_trel (float trel)
    {
        _trel = trel;
    }
    void set_hold (bool hold)
    {
	_hold = hold;
    }
    void set_ambi (bool ambi)
    {
	_ambi = ambi;
    }
    void get_gain (float *gmin, float *gmax)
    {
	*gmin = _gmin * 8.68589f;
	*gmax = _gmax * 8.68589f;
	_newg = true;
    }
    void reset (void);
    void process (int nsamp, float *inp[], float *out[]);

private:

    int               _fsamp;
    int               _nchan;
    float             _ipg;
    float             _ipg1;
    float             _rat;
    float             _rat1;
    float             _pthr;
    float             _tatt;
    float             _trel;
    bool              _hold;
    bool              _ambi;
    float             _za1;
    float             _zr1;
    float             _zr2;
    volatile bool     _newg;
    volatile float    _gmax;
    volatile float    _gmin;
};


#endif
