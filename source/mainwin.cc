// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "styles.h"
#include "global.h"
#include "mainwin.h"


Mainwin::Mainwin (X_rootwin *parent, X_resman *xres, int xp, int yp, Jclient *jclient) :
    A_thread ("Main"),
    X_window (parent, xp, yp, XSIZE, YSIZE, XftColors [C_MAIN_BG]->pixel),
    _stop (false),
    _xres (xres),
    _jclient (jclient),
    _gmeter (0)
{
    X_hints     H;
    int         i;
    char        s [256];

    _atom = XInternAtom (dpy (), "WM_DELETE_WINDOW", True);
    XSetWMProtocols (dpy (), win (), &_atom, 1);
    _atom = XInternAtom (dpy (), "WM_PROTOCOLS", True);

    sprintf (s, "%s", jclient->jack_name ());
    x_set_title (s);
    H.position (xp, yp);
    H.minsize (XSIZE, YSIZE);
    H.maxsize (XSIZE, YSIZE);
    H.rname (xres->rname ());
    H.rclas (xres->rclas ());
    x_apply (&H); 

    _button [B_HLD] = new Pbutt1 (this, this, B_HLD, hldbutt, 340, 42, 32, 18);
    _button [B_AMB] = new Pbutt1 (this, this, B_AMB, ambbutt, 385, 42, 32, 18);
    for (i = 0; i < NBUTTON; i++) _button [i]->x_map ();

    RotaryCtl::init (disp ());
    _rotary [R_IPG] = new Rlinctl (this, this, R_IPG, &ipgain_geom,  0, 0, 250, 5, -10.0,  30.0,   0.0);
    _rotary [R_THR] = new Rlinctl (this, this, R_THR, &thresh_geom,  0, 0, 100, 5, -50.0, -10.0, -30.0);
    _rotary [R_RAT] = new Rcratio (this, this, R_RAT, &cratio_geom,  0, 0);
    _rotary [R_ATT] = new Rlogctl (this, this, R_ATT, &attack_geom,  0, 0, 100, 5, 0.001, 0.1, 0.01);
    _rotary [R_REL] = new Rlogctl (this, this, R_REL, &release_geom, 0, 0, 100, 5, 0.030, 3.0, 0.3);
    for (i = 0; i < NROTARY; i++) _rotary [i]->x_map ();

    _gmeter = new Gmeter (this, 340, 8);
    _gmeter->x_map ();
    
    _jclient->dyncom ()->set_ipgain (_rotary [R_IPG]->value ());
    _jclient->dyncom ()->set_thres (_rotary [R_THR]->value ());
    _jclient->dyncom ()->set_ratio (_rotary [R_RAT]->value ());
    _jclient->dyncom ()->set_tatt (_rotary [R_ATT]->value ());
    _jclient->dyncom ()->set_trel (_rotary [R_REL]->value ());
    _jclient->dyncom ()->set_hold (_button [B_HLD]->state () & 1);
    _jclient->dyncom ()->set_ambi (_button [B_AMB]->state () & 1);

    x_add_events (ExposureMask); 
    x_map (); 
    set_time (0);
    inc_time (250000);
}

 
Mainwin::~Mainwin (void)
{
    delete _gmeter;
    RotaryCtl::fini ();
}

 
int Mainwin::process (void)
{
    int e;

    if (_stop) handle_stop ();

    e = get_event_timed ();
    switch (e)
    {
    case EV_TIME:
        handle_time ();
	break;
    }
    return e;
}


void Mainwin::handle_event (XEvent *E)
{
    switch (E->type)
    {
    case Expose:
	expose ((XExposeEvent *) E);
	break;  
 
    case ClientMessage:
        clmesg ((XClientMessageEvent *) E);
        break;
    }
}


void Mainwin::expose (XExposeEvent *E)
{
    if (E->count) return;
    redraw ();
}


void Mainwin::clmesg (XClientMessageEvent *E)
{
    if (E->message_type == _atom) _stop = true;
}


void Mainwin::handle_time (void)
{
    float v0, v1;

    _jclient->dyncom ()->get_gain (&v0, &v1);
    _gmeter->update (v0, v1);
    inc_time (50000);
    XFlush (dpy ());
}


void Mainwin::handle_stop (void)
{
    put_event (EV_EXIT, 1);
}


void Mainwin::handle_callb (int type, X_window *W, XEvent *E)
{
    PushButton *B;
    RotaryCtl  *R;
    int         k;

    switch (type)
    {
    case RotaryCtl::PRESS:
	R = (RotaryCtl *) W;
	k = R->cbind ();
	switch (k)
	{
	default:
	    ;
	}
	break;

    case RotaryCtl::DELTA:
	R = (RotaryCtl *) W;
	k = R->cbind ();
	switch (k)
	{
	case R_IPG:   
            _jclient->dyncom ()->set_ipgain (_rotary [R_IPG]->value ());
	    break;
	case R_THR:   
            _jclient->dyncom ()->set_thres (_rotary [R_THR]->value ());
	    break;
	case R_RAT:
	    _jclient->dyncom ()->set_ratio (_rotary [R_RAT]->value ());
            break;
	case R_ATT:     
            _jclient->dyncom ()->set_tatt (_rotary [R_ATT]->value ());
	    break;
	case R_REL:     
            _jclient->dyncom ()->set_trel (_rotary [R_REL]->value ());
	    break;
	}
	break;

    case PushButton::PRESS:
	B = (PushButton *) W;
	k = B->cbind ();
	switch (k)
	{
	case B_HLD:    
            _jclient->dyncom ()->set_hold (_button [B_HLD]->state () & 2);
	    break;
	case B_AMB:    
            _jclient->dyncom ()->set_ambi (_button [B_AMB]->state () & 2);
	    break;
	}
    }
}


void Mainwin::redraw (void)
{
    XPutImage (dpy (), win (), dgc (), parsect, 0, 0, 0, 0,  330, 75);
    XPutImage (dpy (), win (), dgc (), redzita, 0, 0, XSIZE - 35, 0, 35, 35);
}


