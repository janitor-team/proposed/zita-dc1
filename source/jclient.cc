// ----------------------------------------------------------------------------
//
//  Copyright (C) 2010-2017 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include <string.h>
#include "jclient.h"


Jclient::Jclient (const char *jname, const char *jserv, int nchan) :
    A_thread ("Jclient"),
    _client (0),
    _clname (0),
    _dyncom (0),
    _active (false)
{
    init_jack (jname, jserv, nchan);   
}


Jclient::~Jclient (void)
{
    if (_client) close_jack ();
    delete _dyncom;
}


void Jclient::init_jack (const char *jname, const char *jserv, int nchan)
{
    int            i;
    char           s [16];
    jack_status_t  stat;
    int            opts;

    opts = JackNoStartServer;
    if (jserv) opts |= JackServerName;
    if ((_client = jack_client_open (jname, (jack_options_t) opts, &stat, jserv)) == 0)
    {
        fprintf (stderr, "Can't connect to JACK\n");
        exit (1);
    }
    jack_set_process_callback (_client, jack_static_process, (void *) this);
    jack_on_shutdown (_client, jack_static_shutdown, (void *) this);
    if (jack_activate (_client))
    {
        fprintf(stderr, "Can't activate JACK.\n");
        exit (1);
    }

    if (nchan < 1) nchan = 1;
    if (nchan > MAXCH) nchan = MAXCH;
    _nchan = nchan;
    _fsamp = jack_get_sample_rate (_client);
    _clname = jack_get_client_name (_client);
    _dyncom = new Dyncom1 (_fsamp, _nchan);
    
    for (i = 0; i < _nchan; i++)
    {
	sprintf (s, "in_%d", i + 1);
        _inpports [i] = jack_port_register (_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
	sprintf (s, "out_%d", i + 1);
        _outports [i] = jack_port_register (_client, s, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    }
    _active = true;
}


void Jclient::close_jack ()
{
    jack_deactivate (_client);
    jack_client_close (_client);
}


void Jclient::jack_static_shutdown (void *arg)
{
    ((Jclient *) arg)->jack_shutdown ();
}


int Jclient::jack_static_process (jack_nframes_t nframes, void *arg)
{
    return ((Jclient *) arg)->jack_process (nframes);
}


void Jclient::jack_shutdown (void)
{
    send_event (EV_EXIT, 1);
}


int Jclient::jack_process (int frames)
{
    int    i;
    float  *inp [MAXCH];
    float  *out [MAXCH];

    if (!_active) return 0;
    for (i = 0; i < _nchan; i++)
    {
	inp [i] = (float *) jack_port_get_buffer (_inpports [i], frames);
	out [i] = (float *) jack_port_get_buffer (_outports [i], frames);
    }
    _dyncom->process (frames, inp, out);
    return 0;
}

